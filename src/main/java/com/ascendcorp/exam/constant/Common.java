package com.ascendcorp.exam.constant;

import org.springframework.stereotype.Component;

@Component
public class Common {

    public static final String VALUE_COLON = ":";

    public static final String VALUE_RESPONSE_APPROVED = "approved";
    public static final String VALUE_RESPONSE_INVALID_DATA = "invalid_data";
    public static final String VALUE_RESPONSE_TRANSACTION_ERROR = "transaction_error";
    public static final String VALUE_RESPONSE_UNKNOWN = "unknown";

    public static final String VALUE_REASON_CODE_200 = "200";
    public static final String VALUE_REASON_CODE_400 = "400";
    public static final String VALUE_REASON_CODE_500 = "500";
    public static final String VALUE_REASON_CODE_501 = "501";
    public static final String VALUE_REASON_CODE_503 = "503";
    public static final String VALUE_REASON_CODE_504 = "504";

    public static final String VALUE_REASON_DESC_GENERAL_INVALID_DATA = "General Invalid Data";
    public static final String VALUE_REASON_DESC_GENERAL_TRANSACTION_ERROR = "General Transaction Error";
    public static final String VALUE_REASON_DESC_ERROR_TIMEOUT = "Error timeout";
    public static final String VALUE_REASON_DESC_INTERNAL_APP_ERROR = "Internal Application Error";




}
